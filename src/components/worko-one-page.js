import { scrollIt, mobileAndTabletCheck } from './../utils';
import imagesloaded from 'imagesloaded';

export default class WorkoOnePage {
  isMobile = false;

  scroll = {
    last: 0,
    down: true
  };

  onePage = {
    el: null,
    scrollMode: false,
    slideEls: null,
    slides: [],
    wrapper: null,
    currentSlide: 0,
    preventScrollEvents: false,
    backTimeout: null,
    sensibility: 20,
    mobileSensibility: 75,
    transitionTime: 500,
    pagination: null,
    bezierEasing: { x1: 0, y1: 0, x2: 1, y2: 1},
    //bezierEasingMobile: { x1: 0.80, y1: 0.00, x2: 0.83, y2: 0.83},
    bezierEasingMobile: { x1: 0, y1: 1, x2: 0.75, y2: 1},
    options: {
      customPagination: false
    }
  };


  constructor(el, options = null) {
    this.onePage.el = el;
    this.onePage.wrapper = this.onePage.el.querySelector('.wonepage-wrapper');
    this.isMobile = mobileAndTabletCheck();
    this.setOptions(options);
    if(this.onePage.scrollMode || this.isMobile) {
      this.onePage.wrapper.style.overflow = 'auto';
    }
    this.setSlides();
    this.setActive(0);
    this.setPagination();
    this.bindListeners();
    this.setReady();
    
  }

  setReady() {
    this.onePage.el.classList.add('wonepage-ready');
  }

  bindListeners() {
    this.bindScrollListener();
    document.addEventListener('keydown', (ev) => {
      if (ev.key === 'ArrowDown' && this.onePage.currentSlide + 1 < this.onePage.slides.length) {
        this.next();
      }
      if (ev.key === 'ArrowUp' && this.onePage.currentSlide > 0) {
        this.prev();
      }
    })

    window.addEventListener('resize', () => {
      this.setSlides();
    })

    imagesloaded(document, () => {
      console.log('Images loaded');
      this.setSlides();
    });
  }

  bindScrollListener() {

    if(this.onePage.scrollMode || this.isMobile) {
      this.onePage.wrapper.addEventListener('scroll', () => {
        this.scroll.down = this.scroll.last < this.onePage.wrapper.scrollTop ? true : false;
        this.scroll.last = this.onePage.wrapper.scrollTop;
        if (!this.onePage.preventScrollEvents) {
          if (this.scroll.down) {
            this.checkNext();
          }
          else {
            this.checkPrev();
          }
        }
      }, { pasive: true });
    }
    else {
      this.onePage.wrapper.addEventListener('wheel', (ev) => {
        this.scroll.down = ev.deltaY > 0 ? true : false;
        const delta = Math.abs(ev.deltaY);
        if (!this.onePage.preventScrollEvents) {
          if (this.scroll.down) {
            this.checkNextWheel(delta);
          }
          else {
            this.checkPrevWheel(delta);
          }
        }
      });
    }
  }

  bindPaginationListeners() {
    const bullets = this.pagination.querySelectorAll('li');
    for (let bullet of bullets) {
      bullet.addEventListener('click', (ev) => {
        const target = parseInt(bullet.getAttribute('data-target'));
        this.unsetActive();
        this.setOut(this.onePage.currentSlide);
        this.onePage.currentSlide = target;
        this.setIn(this.onePage.currentSlide);
        this.setActive(this.onePage.currentSlide);
        this.setActiveBullet(this.onePage.currentSlide);
        this.slide(this.onePage.slides[this.onePage.currentSlide].offsetTop);
        const event = new CustomEvent('slideChange', { detail: { current: this.onePage.currentSlide}})
        this.onePage.el.dispatchEvent(event);
      });
    }

  }

  setOptions(options) {
    if (options.customPagination) { this.onePage.options.customPagination = options.customPagination;}
    if (options.transitionTime) { this.onePage.transitionTime = options.transitionTime;}
    if (options.bezierEasing) { this.onePage.bezierEasing = options.bezierEasing;}
    if (options.scrollMode) { this.onePage.scrollMode = options.scrollMode;}
    //Diff sensibility if mobile
    if (this.isMobile) {
      this.onePage.sensibility = this.onePage.mobileSensibility;
    }
    else {
      if (options.sensibility || options.sensibility === 0) { this.onePage.sensibility = options.sensibility;}
    }
    
  }

  setPagination() {
    this.pagination = this.onePage.el.querySelector('.wonepage-pagination');
    if (this.pagination) {
      let paginationInnerHTML = '<ul>';
      let customPagination;
      for (let i = 0; i < this.onePage.slides.length; i++) {
        customPagination = this.pagination.children[i];
        paginationInnerHTML += `<li class="wonepage-pagination-item" data-target="${i}">${this.onePage.options.customPagination ? (customPagination ? customPagination.outerHTML : '') : ''}<span class="wonepage-pagination-bullet"></span></li>`;
      }
      this.paginationInnerHTML += '</ul>';
      this.pagination.innerHTML = paginationInnerHTML;
      this.bindPaginationListeners();
      this.setActiveBullet(0);
    }
  }

  unsetActive() {
    this.onePage.slideEls[this.onePage.currentSlide].classList.remove('active');
  }

  setActive(slideIndex) {
    this.onePage.slideEls[slideIndex].classList.add('active');
  }

  setIn(slideIndex) {
    this.onePage.slideEls[slideIndex].classList.add('in');
    setTimeout(() => this.onePage.slideEls[slideIndex].classList.remove('in') , this.onePage.transitionTime);
  }

  setOut(slideIndex) {
    this.onePage.slideEls[slideIndex].classList.add('out');
    setTimeout(() => this.onePage.slideEls[slideIndex].classList.remove('out') , this.onePage.transitionTime);
  }

  setActiveBullet(current) {
    const currentBullet = this.pagination.querySelector(`li[data-target="${current}"]`);
    if (currentBullet && !currentBullet.classList.contains('active')) {
      const activeBullets = this.pagination.querySelectorAll('li.active');
      for (let activeBullet of activeBullets) {
        activeBullet.classList.remove('active');
      }
      currentBullet.classList.add('active');
    }
  }

  setSlides() {
    this.onePage.slides = [];
    this.onePage.slideEls = this.onePage.el.querySelectorAll('.wonepage-slide');
    let offsetTop = 0;
    for (let slideEl of this.onePage.slideEls) {
      const slide = {
        height: slideEl.clientHeight,
        overflow: slideEl.clientHeight > this.onePage.wrapper.clientHeight,
        diff: slideEl.clientHeight - this.onePage.wrapper.clientHeight,
        offsetTop: offsetTop
      }
      this.onePage.slides.push(slide);
      offsetTop += slideEl.clientHeight;
    }
  }

  checkNext() {
    if (this.onePage.currentSlide === this.onePage.slideEls.length - 1) return;
    if (this.onePage.backTimeout) clearTimeout(this.onePage.backTimeout);
    const slidePos = this.onePage.wrapper.scrollTop - this.onePage.slides[this.onePage.currentSlide].offsetTop;
    if (slidePos > this.onePage.slides[this.onePage.currentSlide].diff) {
      const nextProgress = slidePos - this.onePage.slides[this.onePage.currentSlide].diff;
      if (nextProgress > this.onePage.sensibility) {
        this.next();
      }
      else {
        this.onePage.backTimeout = setTimeout(() => {
          this.currentBottom();
        }, 50)
      }
    }
  }

  checkPrev() {
    if (this.onePage.currentSlide === 0) return;
    if (this.onePage.backTimeout) clearTimeout(this.onePage.backTimeout);
    const slidePos = this.onePage.wrapper.scrollTop - this.onePage.slides[this.onePage.currentSlide].offsetTop;
    if (slidePos < 0) {
      const prevProgress = slidePos - this.onePage.slides[this.onePage.currentSlide].diff;
      if (Math.abs(prevProgress) > this.onePage.sensibility) {
        this.prev();
      }
      else {
        this.onePage.backTimeout = setTimeout(() => {
          this.currentTop();
        }, 50)
      }
    }
  }

  checkNextWheel(delta) {
    const slidePos = this.onePage.wrapper.scrollTop - this.onePage.slides[this.onePage.currentSlide].offsetTop;
    if(slidePos < this.onePage.slides[this.onePage.currentSlide].diff) {
      this.onePage.wrapper.scrollTop += delta / 2;
    }
    else {
      if(delta > this.onePage.sensibility) {
        if (this.onePage.currentSlide === this.onePage.slideEls.length - 1) return;
        this.next();
      }
    }
  }

  checkPrevWheel(delta) {
    const slidePos = this.onePage.wrapper.scrollTop - this.onePage.slides[this.onePage.currentSlide].offsetTop;
    if(slidePos > 0) {
      this.onePage.wrapper.scrollTop -= delta / 2;
    }
    else {
      if (this.onePage.currentSlide === 0) return;
      if(delta > this.onePage.sensibility) {
        this.prev();
      }
    }
  }

  currentBottom() {
    this.slide(this.onePage.slides[this.onePage.currentSlide].offsetTop + this.onePage.slides[this.onePage.currentSlide].diff);
  }

  currentTop() {
    this.slide(this.onePage.slides[this.onePage.currentSlide].offsetTop);
  }

  next() {
    this.unsetActive();
    this.setOut(this.onePage.currentSlide);
    this.onePage.currentSlide++;
    this.setIn(this.onePage.currentSlide);
    this.setActive(this.onePage.currentSlide);
    this.setActiveBullet(this.onePage.currentSlide);
    this.slide(this.onePage.slides[this.onePage.currentSlide].offsetTop);
    const event = new CustomEvent('slideChange', { detail: { current: this.onePage.currentSlide}})
    this.onePage.el.dispatchEvent(event);
  }

  prev() {
    this.unsetActive();
    this.setOut(this.onePage.currentSlide);
    this.onePage.currentSlide--;
    this.setIn(this.onePage.currentSlide);
    this.setActive(this.onePage.currentSlide);
    this.setActiveBullet(this.onePage.currentSlide);
    this.slide(this.onePage.slides[this.onePage.currentSlide].offsetTop);
    const event = new CustomEvent('slideChange', { detail: { current: this.onePage.currentSlide}})
    this.onePage.el.dispatchEvent(event);
  }

  slide(target, duration) {
    this.onePage.preventScrollEvents = true;
    if (this.isMobile || this.onePage.scrollMode) {
      this.onePage.wrapper.style.overflow = 'hidden';
    }
    scrollIt(target, duration ? duration : this.onePage.transitionTime, this.isMobile ? this.onePage.bezierEasingMobile : this.onePage.bezierEasing, this.onePage.wrapper, () => {
      this.onePage.preventScrollEvents = false;
      if (this.isMobile || this.onePage.scrollMode) {
        this.onePage.wrapper.style.overflowY = 'auto';
      }
    });
  }
}