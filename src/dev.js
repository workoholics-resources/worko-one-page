import "./style.scss";
import WorkoOnePage from './components/worko-one-page';

const onePages = document.querySelectorAll(".wonepage");
for (let onePage of onePages) {
  new WorkoOnePage(onePage, {
    customPagination: true,
    bezierEasing: {x1: 0.4, y1: 0, x2: 0.2, y2: 1},
    transitionTime: 1000
  });

  // Slide change event fired
  onePage.addEventListener('slideChange', (ev) => {
    console.log(ev);
  });

  // One page load event fired
  onePage.addEventListener('test', (ev) => {
    console.log(ev);
  });

}