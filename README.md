# Worko One Page

## Install

```shell
npm i @workoholics/worko-one-page
```

## Usage 

### As ES module

```js
import { WorkoOnePage } from '@workoholics/worko-one-page'

const onePage = document.getElementById('selector');
onePage ? new WorkoOnePage(onePage) : null;
```

### Other method

```html
<script src="node_modules/dist/worko-one-page.min.js"></script>
```

```js
var el = document.getElementById('selector');
el ? new WorkoOnePage(el) : null; 
```

### HTML declaration

```html
  <div class="wonepage">
    <div class="wonepage-pagination"></div>
    <div class="wonepage-wrapper">
      <div class="wonepage-slide">
        <h1>Slide 1</h1>
      </div>
      <div class="wonepage-slide">
        <h1>Slide 2</h1>
      </div>
      <div class="wonepage-slide">
        <h1>Slide 3</h1>
      </div>
    </div>
  </div>
```

### Options

#### Insert custom pagination elements
```js
WorkoOnePage(el, {
  customPagination: true
})
```
```html
  <div class="wonepage">
    <div class="wonepage-pagination">
        <span>Page 1</span>
        <span>Page 2</span>
        <span>Page 3</span>
    </div>
    <div class="wonepage-wrapper">
      <div class="wonepage-slide">
        <h1>Slide 1</h1>
      </div>
      <div class="wonepage-slide">
        <h1>Slide 2</h1>
      </div>
      <div class="wonepage-slide">
        <h1>Slide 3</h1>
      </div>
    </div>
  </div>
```
#### Transition time

Slide transition time in miliseconds

```js
WorkoOnePage(el, {
  transitionTime: 500,
})
```

#### Bezier easing

Transition easing parametrized by bezier curve

```js
WorkoOnePage(el, {
  bezierEasing: {x1: 0, y1: 0, x2: 1, y2: 1}, // linear
})
```

#### Sensibility

Amount of scroll pixels to change the slide

```js
WorkoOnePage(el, {
  sensibility: 100, // linear
})
```

## Events

### Slide change

Is fired when slide changes

```js
onePage.addEventListener('slideChange', (ev) => {
  // Do something
  console.log(ev.detail.current) // Index of the current slide
});
```