const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, options) => { return {
  entry: options.mode === 'production' ? './src/index.js' : './src/dev.js',
  mode: options.mode,
  output: {
    filename: 'worko-one-page.min.js',
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'umd'
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    watchContentBase: true,
    port: 9090,
    hot: true,
    inline: true,
    disableHostCheck: true,
    host: '0.0.0.0'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/, // or /\.css$/i if you aren't using sass
        use: [
          {
            loader: 'style-loader',
            options: { 
                insert: 'head', // insert style tag inside of <head>
                injectType: 'singletonStyleTag' // this is for wrap all your style in just one style tag
            },
          },
          "css-loader",
          "sass-loader"
        ],
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread', "@babel/plugin-proposal-class-properties"]
          }
        }
      }
    ],
  },
  plugins: [new HtmlWebpackPlugin({
    template: 'src/demo.html'
  })]
}};